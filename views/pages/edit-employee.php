<?php
require_once __DIR__."/../../helper/init.php";
$page_title ="Employee Management | Edit EMPLOYEE";
    $sidebarSection = 'edit-emp';

    Util::createCSRFToken();
  $errors="";
  $old="";
  if(Session::hasSession('old'))
  {
    $old = Session::getSession('old');
    Session::unsetSession('old');
  }
  if(Session::hasSession('errors'))
  {
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
  }
?>

<!DOCTYPE html>
<html lang="en">

<?php
          require_once __DIR__."/../includes/head-section.php";
?> 

<body class="">
  <div class="wrapper ">
    <!-- require SB -->
    <?php
          require_once __DIR__."/../includes/sidebar.php";
      ?> 
    <div class="main-panel">
      <!-- Navbar -->
      <?php
          require_once __DIR__."/../includes/navbar.php";
      ?> 
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-10">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Edit Employee</h4>
                  <p class="card-category">Edit your profile</p>
                </div>
                <div class="card-body">
                <form action="<?= BASEURL?>helper/routing.php" method="POST" id="edit-employee">
                <input type="hidden" name="id" id="edit_employee_id" value="<?= $_POST['id']?>">
                <input type="hidden"
                              name="csrf_token"
                              value="<?= Session::getSession('csrf_token');?>">
                    <div class="row">
                  
                      <div class="col-md-6">
                        <div class="form-group">
                        <label for="username" class="bmd-label-floating">Username</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('username') ? 'error is-invalid' : '') : '';?>"
                                    name="username"
                                    id="username"  
                                    
                                    value="<?= $old != '' ?$old['username']: $_POST['username'];?>"
                                  >
                                  <?php
                                if($errors!="" && $errors->has('username')):
                                  echo "<span class='error'> {$errors->first('username')}</span>";
                                endif;
                                ?>      
                                 
                        </div>
                       
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                        <label for="email" class="bmd-label-floating">Email</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('email') ? 'error is-invalid' : '') : '';?>"
                                    name="email"
                                    id="email"  
                                    value= <?= $old != '' ?$old['email']: $_POST['email']?>
                                  >
                                  <?php
                                if($errors!="" && $errors->has('email')):
                                  echo "<span class='error'> {$errors->first('email')}</span>";
                                endif;
                                ?>
                                 
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                        <label for="first_name" class="bmd-label-floating">First Name</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('first_name') ? 'error is-invalid' : '') : '';?>"
                                    name="first_name"
                                    id="first_name"  
                                    value= <?= $old != '' ?$old['first_name']: $_POST['first_name']?>
                                  >
                                  <?php
                                if($errors!="" && $errors->has('first_name')):
                                  echo "<span class='error'> {$errors->first('first_name')}</span>";
                                endif;
                                ?>
                                  
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                        <label for="last_name" class="bmd-label-floating">Last Name</label>
                                  <input type="text" 
                                  class="form-control <?= $errors!= '' ? ($errors->has('last_name') ? 'error is-invalid' : '') : '';?>"
                                   name="last_name"
                                    id="last_name"  
                                    value= <?= $old != '' ?$old['last_name']: $_POST['last_name']?>
                                  >
                                  <?php
                                if($errors!="" && $errors->has('last_name')):
                                  echo "<span class='error'> {$errors->first('last_name')}</span>";
                                endif;
                                ?>
                        </div>  
                                  
                      </div>

                      <div class="col-md-12">
                        <div class="form-group ">
                        <label for="address" class="bmd-label-floating">Address</label>
                                  <textarea rows="2" 
                                    class="form-control  <?= $errors!= '' ? ($errors->has('address') ? 'error is-invalid' : '') : '';?>"
                                    name="address"
                                    id="address"  
                                    
                                  ><?= $old != '' ?$old['address']: $_POST['address']?></textarea>
                                  <?php
                                if($errors!="" && $errors->has('address')):
                                  echo "<span class='error'> {$errors->first('address')}</span>";
                                endif;
                                ?>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                        <label for="city" class="bmd-label-floating">City</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('city') ? 'error is-invalid' : '') : '';?>"
                                    name="city"
                                    id="city"  
                                    value= <?= $errors!= '' ? ($errors->has('city') ? 'error is-invalid' : '') :  $_POST['city']?>
                                  >
                                  <?php
                                if($errors!="" && $errors->has('city')):
                                  echo "<span class='error'> {$errors->first('city')}</span>";
                                endif;
                                ?>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                        <label for="country" class="bmd-label-floating">Country</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('country') ? 'error is-invalid' : '') : '';?>"
                                    name="country"
                                    id="country"  
                                    value= <?= $old != '' ?$old['country']:  $_POST['country']?>
                                  >
                                  <?php
                                if($errors!="" && $errors->has('country')):
                                  echo "<span class='error'> {$errors->first('country')}</span>";
                                endif;
                                ?>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                        <label for="postal_code" class="bmd-label-floating">Postal Code</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('postal_code') ? 'error is-invalid' : '') : '';?>"
                                    name="postal_code"
                                    id="postal_code"  
                                    value= <?= $old != '' ?$old['postal_code']: $_POST['postal_code']?>
                                  >
                                  <?php
                                if($errors!="" && $errors->has('postal_code')):
                                  echo "<span class='error'> {$errors->first('postal_code')}</span>";
                                endif;
                                ?>
                                  
                                
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                        <label for="company_branch" class="bmd-label-floating">Company Branch</label>
                                  <input type="text" 
                                    class="form-control <?= $errors!= '' ? ($errors->has('company_branch') ? 'error is-invalid' : '') : '';?>"
                                    name="company_branch"
                                    id="company_branch"  
                                    value= <?= $old != '' ?$old['company_branch']: $_POST['company_branch']?>
                                  >
                                  <?php
                                if($errors!="" && $errors->has('company_branch')):
                                  echo "<span class='error'> {$errors->first('company_branch')}</span>";
                                endif;
                                ?>
                        </div>
                      </div>

                      <div class="col-md-12">
                        <div class="form-group">
                        <label for="about" class="bmd-label-floating">About Me</label>
                                  <textarea rows="2"
                                    class="form-control"
                                    name="about"
                                    id="about"  
                                    
                                  ><?= $old != '' ?$old['about']: $_POST['about']?></textarea>
                                  <?php
                                ?>
                        </div>
                      </div>
                    <div class="col-md-12">
                    <input type="submit" class="btn btn-primary pull-right" name="editEmployee" value="Edit Employee">
                    
                    </div>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
    require_once __DIR__."/../includes/footer.php";
    ?> 
      <!-- req footer -->
    </div>
    
  </div>
  
  </div>
  <!--   Core JS Files   -->
  <?php
          require_once __DIR__."/../includes/page-level/index-scripts.php";
          
      ?> 
<script src="<?=BASEASSETS?>js/pages/edit-employee.js"></script>
</body>

</html>