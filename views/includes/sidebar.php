<div class="sidebar" data-color="purple" data-background-color="white" data-image="<?=BASEASSETS?>;/img/sidebar-1.jpg">
      <div class="logo"><a  class="simple-text logo-normal">
          Employee Management
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item <?= $sidebarSection =='dashboard' ? 'active' : '';?>  ">
            <a class="nav-link" href="./dashboard.php">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item <?= $sidebarSection =='add-emp' ? 'active' : '';?>">
            <a class="nav-link" href="./add-employee.php">
              <i class="material-icons">person</i>
              <p>Add Employee</p>
            </a>
          </li>
          <li class="nav-item <?= $sidebarSection =='manage-emp' ? 'active' : '';?>">
            <a class="nav-link" href="./manage-employee.php">
              <i class="material-icons">settings</i>
              <p>Manage Employee</p>
            </a>
          </li>
        </ul>
      </div>
</div>