var TableDatatables = function(){
    var handleEmployeeTable = function(){
        var manageEmployeeTable = $("#manage-employee-table");
        var baseURL = window.location.origin;
        
        var filePath = "/helper/routing.php";
        console.log(baseURL+filePath);
        var oTable = manageEmployeeTable.DataTable({
            "processing":true,
            "serverSide":true,
            "ajax":{
                url:baseURL+filePath,
                type:"POST",
                data:{
                    "page": "manage-employee"
                }
            },
            "lengthMenu": [
                [5,15,25,-1],
                [5,15,25,"All"]
            ],
            "order": [
                [1,"desc"]
            ],
            "columnDefs": [
                {
                    'orderable': false,
                    'targets': [0,-1]
                }
            ]
        });
        var manFunc = manageEmployeeTable.on('click','.edit', function(e){
                                        
            var id = $(this).data('id');
            // console.log($(this).data('id'));
            $("#edit_employee_id").val(id);
            //Fetching all other values from the database `using AJAX ombimand loading them onto thier respective fields in the modal.
            $.ajax({
                url:baseURL+filePath,
                method:"POST",
                data:{
                    "employee_id":id,
                    "fetch":"employee"
                },
                dataType:"json",
                success:function(data){
                   
                        var form = document.createElement('form');
                        document.body.appendChild(form);
                        form.method = 'post';
                        form.action = './edit-employee.php';
                        for (var name in data) {
                            var input = document.createElement('input');
                            input.type = 'hidden';
                            input.name = name;
                            input.value = data[name];
                            form.appendChild(input);
                        }
                        form.submit();
                }           
            })
        });        
    }
    return{
        //main function to handle all the datatables

        init: function(){
            handleEmployeeTable();
        }
    }
}();

jQuery(document).ready(function(){
    TableDatatables.init();
});


/**
 * 
 * manage-customer.php
 * manage-customer-scripts.php
 * manage-employee.js
 * 
 * ui mai category jaisa Customers daalna hy and usmai add and manage
 * 
 */