$(function(){
    $("#add-employee").validate({
        rules: {
            
            'company_branch' : {
                required : true,
                minlength : 3,
                maxlength : 50,
            },
            'username' :{
                required : true,
                minlength : 3,
                maxlength : 25,
            },
            'email' :{
                required : true,
                minlength : 3,
                maxlength : 50,
            },
            'first_name' :{
                required : true,
                minlength : 3,
                maxlength : 25,
            },
            'last_name' :{
                required : true,
                minlength : 3,
                maxlength : 25,
            },
            'address' :{
                required : true,
                minlength : 3,
                maxlength : 255
            },
            'city' :{
                required : true,
                minlength : 2,
                maxlength : 25,
            },
            'country' :{
                required : true,
                minlength : 3,
                maxlength : 40,
            },
            'postal_code' :{
                required : true,
                minlength : 4,
                maxlength : 15,
            }
        },
        submitHandler: function (form){
            form.submit();
        }
    })
});