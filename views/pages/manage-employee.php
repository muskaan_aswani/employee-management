<?php
require_once __DIR__. "/../../helper/init.php";
$page_title = "Employee Management | Manage Employee";
$sidebarSection = 'manage-emp';
?>
<!DOCTYPE html>
<html lang="en">
      <?php
          require_once __DIR__."/../includes/head-section.php";
      ?> 

<link rel="stylesheet" href="<?=BASEASSETS;?>css/plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="<?=BASEASSETS;?>vendor/datatables/datatables.min.css">
<body class="">
  <div class="wrapper ">
  <?php
          require_once __DIR__."/../includes/sidebar.php";
      ?> 
    <div class="main-panel">
      <!-- Navbar -->
      <?php
          require_once __DIR__."/../includes/navbar.php";
      ?> 
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
         
          <div class="row">
          
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Employee Details</h4>
                  
                </div>
                <div class="card-body">
                  <div>
                  <table class="table table-bordered table-responsive" id="manage-employee-table">
                        <thead>
                          <style>
    
                            th{
                              font-size:1rem;
                              width: 10%;
                            }
                          </style>
                                <tr>
                                    <th>#</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>City</th>
                                    <th>Country</th>
                                    <th><span>Postal Code</span></th>
                                    <th>Company Branch</th>
                                    <th>About</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        
      </div>

      
      <!-- req foot -->
      <?php
          require_once __DIR__."/../includes/footer.php";
      ?> 
    </div>
  </div>
  
  <!--   Core JS Files   -->
  <?php
          require_once __DIR__."/../includes/page-level/index-scripts.php";
      ?> 
      <?php require_once __DIR__."/../includes/page-level/manage-employee-scripts.php"; ?>
      

</body>

</html>