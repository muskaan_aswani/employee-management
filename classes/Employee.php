<?php
class Employee
{
    private $table = "emp_details";
    private $columns = ['id','company_branch','username','email','first_name','last_name','address','city','country','postal_code','about'];
    protected $di;
    private $database;
    private $validator;

    public function __construct(DependencyInjector $di)
    {
        $this->di = $di;
        $this->database = $this->di->get('database');
    }

    public function getValidator(){
        return $this->validator;
    }

    public function validateData($data)
    {
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data,[
            'company_branch' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 50,
            ],
            'username' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 25,
                'unique' =>$this->table
            ],
            'email' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 50,
                'unique' =>$this->table
            ],
            'first_name' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 25,
                'unique' =>$this->table
            ],
            'last_name' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 25,
                'unique' =>$this->table
            ],
            'address' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 255
            ],
            'city' =>[
                'required' => true,
                'minlength' => 2,
                'maxlength' => 25,
            ],
            'country' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 40,
            ],
            'postal_code' =>[
                'required' => true,
                'minlength' => 4,
                'maxlength' => 15,
            ]
        ]);
    }

    public function validateEditData($data)
    {
        $this->validator = $this->di->get('validator');
        $this->validator = $this->validator->check($data,[
            'company_branch' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 50,
            ],
            'username' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 25,
                
            ],
            'email' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 50,
                
            ],
            'first_name' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 25,
                
            ],
            'last_name' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 25,
                
            ],
            'address' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 255
            ],
            'city' =>[
                'required' => true,
                'minlength' => 2,
                'maxlength' => 25,
            ],
            'country' =>[
                'required' => true,
                'minlength' => 3,
                'maxlength' => 40,
            ],
            'postal_code' =>[
                'required' => true,
                'minlength' => 4,
                'maxlength' => 15,
            ]
        ]);
    }

    public function addEmployee($data)
    {
        //VALIDATE DATA

        $this->validateData($data);

        //INSERT DATA IN DATABSE

        if(!$this->validator->fails())
        {
            try
            {
                $this->database->beginTransaction();

                $data_to_be_inserted=[
                    'company_branch'=>$data['company_branch'],
                    'username' => $data['username'],
                    'email'=> $data['email'], 
                    'first_name' =>  $data['first_name'],
                    'last_name' =>  $data['last_name'],
                    'address' => $data['address'],
                    'city' => $data['city'],
                    'country' => $data['country'],
                    'postal_code' => $data['postal_code'],
                    'about' =>  $data['about'],

                ];
                
                

                $employee_id =$this->database->insert($this->table,$data_to_be_inserted);
                $this->database->commit();
                
                return ADD_SUCCESS;

            }catch(Exception $e){
                $this->database->rollBack();
                return ADD_ERROR;
            }
            
        }

        return VALIDATION_ERROR;

        //RETURN THE STATUS OF OPERATION
    }
    public function getJSONDataForDataTable($draw, $search_parameter,$order_by,$start,$length)
    {
        $query = "SELECT * FROM {$this->table} WHERE deleted = 0";
        $totalRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";
        $filteredRowCountQuery = "SELECT COUNT(*) as total_count FROM {$this->table} WHERE deleted = 0";

        if($search_parameter != null)
        {
            $query .= " AND (first_name LIKE '%{$search_parameter}%' OR last_name LIKE '%{$search_parameter}%') ";
            $filteredRowCountQuery .= " AND (first_name LIKE '%{$search_parameter}%' OR first_name LIKE '%{$search_parameter}%')";
        }
        // UTIL::dd($this->columns[$order_by[0]['column']]);
        if($order_by != null)
        {
            $query .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[$order_by[0]['column']]} {$order_by[0]['dir']}";
        }
        else{
            $query .= " ORDER BY {$this->columns[0]} ASC";
            $filteredRowCountQuery .= " ORDER BY {$this->columns[0]} ASC";
        }

        if($length != -1)
        {
            $query .= " LIMIT {$start}, {$length}";
        }
        $totalRowCountResult = $this->database->raw($totalRowCountQuery);
        $numberOfTotalRows = is_array($totalRowCountResult) ? $totalRowCountResult[0]->total_count : 0;

        $filteredRowCountResult = $this->database->raw($filteredRowCountQuery);
        $numberOfFilteredRows = is_array($filteredRowCountResult) ? $filteredRowCountResult[0]->total_count : 0;


        $fetchedData = $this->database->raw($query);
        $data = [];
        $baseassets=BASEASSETS;
        $numRows = is_array($fetchedData) ? count($fetchedData) : 0;
        for($i=0; $i<$numRows; $i++)
        {
            //$name = $fetchedData[$i]->first_name ." ".$fetchedData[$i]->last_name;
            
            $subArray = [];
            $subArray[] = $start+$i+1;
            $subArray[] = $fetchedData[$i]->username;
            $subArray[] = $fetchedData[$i]->email;
            $subArray[] = $fetchedData[$i]->first_name." ".$fetchedData[$i]->last_name;
            $subArray[] = $fetchedData[$i]->address;
            $subArray[] = $fetchedData[$i]->city;
            $subArray[] = $fetchedData[$i]->country;
            $subArray[] = $fetchedData[$i]->postal_code;
            $subArray[] = $fetchedData[$i]->company_branch;
            $subArray[] = $fetchedData[$i]->about;
            $subArray[] = <<<BUTTONS
<form  action="{$baseassets}../helper/routing.php" method="POST">
<input type="hidden" name="employee_id" value="{$fetchedData[$i]->id}">
<button class='btn btn-outline-primary btn-sm edit' name="edit_data" data-id='{$fetchedData[$i]->id}'><i class="fas fa-pencil-alt"></i></a></button>
<button class='btn btn-outline-danger btn-sm' data-id='{$fetchedData[$i]->id}'><i class="fas fa-trash-alt"></i></button>   
</form>    
BUTTONS;

            $data[] = $subArray;
        }

        $output = array(
            'draw'=>$draw,   //gives page no.
            'recordsTotal'=>$numberOfTotalRows,
            'recordsFiltered'=>$numberOfFilteredRows,
            'data'=>$data
        );
        echo json_encode($output);
    }

    public function getEmployeeByID($id, $fetchMode = PDO::FETCH_OBJ)
    {
        $query = "SELECT * FROM {$this->table} WHERE id = {$id} AND deleted = 0";
        $result = $this->database->raw($query,$fetchMode);
        return $result;
    }
    public function update($data,$id)
    {
        $this->validateEditData($data);
        // Util::dd($id);
        

        if(!$this->validator->fails())
     
        {
            try{
                $this->database->beginTransaction();
                $data_to_be_updated=[
                    'company_branch'=>$data['company_branch'],
                    'username' => $data['username'],
                    'email'=> $data['email'], 
                    'first_name' =>  $data['first_name'],
                    'last_name' =>  $data['last_name'],
                    'address' => $data['address'],
                    'city' => $data['city'],
                    'country' => $data['country'],
                    'postal_code' => $data['postal_code'],
                    'about' =>  $data['about'],

                ];
               
                $this->database->update($this->table,$data_to_be_updated,"id = {$id}");
                $this->database->commit();
                return UPDATE_SUCCESS;
            }catch(Exception $e){
                $this->database->rollBack();
                return UPDATE_ERROR;
            }
        }
        else{
            return VALIDATION_ERROR;
        }
        
    }
}
?>
