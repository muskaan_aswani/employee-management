-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2020 at 04:23 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emp_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `emp_details`
--

CREATE TABLE `emp_details` (
  `id` int(255) NOT NULL,
  `company_branch` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(25) NOT NULL,
  `country` varchar(25) NOT NULL,
  `postal_code` int(15) NOT NULL,
  `about` varchar(255) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_details`
--

INSERT INTO `emp_details` (`id`, `company_branch`, `username`, `email`, `first_name`, `last_name`, `address`, `city`, `country`, `postal_code`, `about`, `deleted`, `created_time`, `updated_time`) VALUES
(2, 'Dadar', 'ishajoglekar', 'isha@sl.com', 'isha', 'joglekar', '202 Ramanand Society', 'dombivli', 'India', 421201, 'IT Student', 0, '2020-05-15 02:09:04', '2020-05-15 02:09:04'),
(3, 'Bandra', 'MuskaanAswani', 'muskan@gmail.com', 'Muskaan', 'Aswani', 'Ulhas', 'Ulhas', 'India', 421001, 'CSE Student', 0, '2020-05-15 02:13:37', '2020-05-15 02:13:37'),
(4, 'USA', 'John', 'john@sl.com', 'John', 'Doe', 'IUPUI', 'NY', 'USA', 97076, 'gduvedn k;enlevjoejbwvpwvkip[emv knbguwosjkc,v0wqu9hicn', 0, '2020-05-15 02:58:58', '2020-05-15 02:58:58'),
(5, 'Thane', 'Jane', 'jane@gmail.com', 'Jane', 'Doey', 'abc', 'xyz', 'INDIA', 2456789, 'Just added my about me!', 0, '2020-05-15 20:05:50', '2020-05-15 20:05:50'),
(6, 'Body', 'frenchfries', 'frenchfries@gmail.com', 'Fench', 'Fries', 'Tummy', 'Abdomen', 'Muskan', 1234567, 'i am very tasty and spicy!!!', 0, '2020-05-16 01:29:07', '2020-05-16 01:29:07'),
(7, 'mumbai', 'Ria', 'ria@gmail.com', 'ria', 'vavhal', 'dadar east', 'mumbai', 'india', 123789, 'Design Student', 0, '2020-05-17 19:48:24', '2020-05-17 19:48:24'),
(8, 'vowels/cons', 'abc17', 'abc@gmail.com', 'abc', 'xyz', 'alphabets', 'letters', 'english', 12344, 'I am very funny!\r\n', 0, '2020-05-17 19:50:58', '2020-05-17 19:50:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `emp_details`
--
ALTER TABLE `emp_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `emp_details`
--
ALTER TABLE `emp_details`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
