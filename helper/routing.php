<?php
require_once 'init.php';
if(isset($_POST['add_employee']))
{
    if(Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('employee')->addEmployee($_POST);
        //Util::dd($_POST);
        switch($result)
        {
            
            case ADD_ERROR:
                Session::setSession(ADD_ERROR,"Add Employee Error!");
                Util::redirect("manage-employee.php");
            break;
            case ADD_SUCCESS:
                Session::setSession(ADD_SUCCESS,"Add Employee Success!");
                Util::redirect("manage-employee.php");
            break;
            case VALIDATION_ERROR:
                Session::setSession('validation',"Validation Error");
                Session::setSession('old',$_POST);
                Session::setSession('errors', serialize($di->get('employee')->getValidator()->errors()));
                Util::redirect("add-employee.php");
            break;
        }
    }
    else{
        Session::setSession("csrf","CSRF ERROR");
        Util::redirect("manage-employee.php"); //Need to change,actualy we will be redirecting to error page, indicating unauthorized access
    }
}
// if(isset($_POST['add_customer']))
// {
//     if(Util::verifyCSRFToken($_POST))
//     {
//         $result = $di->get('customer')->addCustomer($_POST);
//         //Util::dd($_POST);
//         switch($result)
//         {
//             case ADD_ERROR:
//                 Session::setSession(ADD_ERROR,"Add Customer Error!");
//                 Util::redirect("manage-customer.php");
//             break;
//             case ADD_SUCCESS:
//                 Session::setSession(ADD_SUCCESS,"Add Customer Success!");
//                 Util::redirect("manage-customer.php");
//             break;
//             case VALIDATION_ERROR:
//                 Session::setSession('validation',"Validation Error");
//                 Session::setSession('old',$_POST);
//                 Session::setSession('errors', serialize($di->get('customer')->getValidator()->errors()));
//                 Util::redirect("add-customer.php");
//             break;
//         }
//     }
//     else{
//         Session::setSession("csrf","CSRF ERROR");
//         Util::redirect("manage-category.php"); //Need to change,actualy we will be redirecting to error page, indicating unauthorized access
//     }
// }
if(isset($_POST['page']))
{
    //$_POST['search]['value']
    //$_POST['start']
    //$_POST['length']
    
    if($_POST['page'] == 'manage-employee')
    {
        $dependency = "employee";
    }
    elseif($_POST['page'] == 'manage-customer')
    {
        $dependency = "customer";
    }

    $search_parameter = $_POST['search']['value'] ?? null;
    $order_by = $_POST['order'] ?? null;
    $start = $_POST['start'];
    $length = $_POST['length'];
    $draw = $_POST['draw'];
    $di->get($dependency)->getJSONDataForDataTable($draw, $search_parameter, $order_by , $start, $length);
}


if(isset($_POST['fetch']))
{
    // Util::dd($_POST);
    if($_POST['fetch'] =='employee')
    {
        
        $employee_id = $_POST['employee_id'];
        $result = $di->get('employee')->getEmployeeByID($employee_id, PDO::FETCH_ASSOC);
        echo json_encode($result[0]);
        
        
    }
}   

// if(isset($_POST['edit_data']))
// {
//     $employee_id = $_POST['employee_id'];
//     $result = $di->get('employee')->getEmployeeByID($employee_id, PDO::FETCH_ASSOC);
//     echo json_encode($result[0]);
//     Util::redirect("edit-employee.php");
    
// }


if(isset($_POST['editEmployee']))
{
    // Util::dd($_POST);
    
    if(Util::verifyCSRFToken($_POST))
    {
        $result = $di->get('employee')->update($_POST,$_POST['id']);
        
        switch($result)
        {
            case UPDATE_ERROR:
                Session::setSession(UPDATE_ERROR,"Update Employee Error!");
                Util::redirect("edit-employee.php");
            break;
            case UPDATE_SUCCESS:
                Session::setSession(UPDATE_SUCCESS,"Update employee Success!");
                Util::redirect("manage-employee.php");
            break;
            case VALIDATION_ERROR:
                Session::setSession('validation',"Validation Error");
                Session::setSession('old',$_POST);
                Session::setSession('errors', serialize($di->get('employee')->getValidator()->errors()));
                Util::redirect("edit-employee.php");
            break;
        }
    }
    else{
        Session::setSession("csrf","CSRF ERROR");
        Util::redirect("manage-employee.php"); //Need to change,actualy we will be redirecting to error page, indicating unauthorized access
    }
}